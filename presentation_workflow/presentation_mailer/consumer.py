import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='presentation_approvals')
        channel.queue_declare(queue='presentation_rejections')

        def process_approval(ch, method, properties, body):
            context = json.loads(body)
            send_mail(
                "Presentation approved!!",
                message=f" The following presentation was approved:{context['title']}",
                from_email="foo@bar.com",
                recipient_list=[context['presenter_email']],
                fail_silently=False,
            )

        def process_rejection(ch, method, properties, body):
            context = json.loads(body)
            send_mail(
                "Presentation denied!!",
                message=f" The following presentation was rejected: {context['title']}",
                from_email="foo@bar.com",
                recipient_list=[context['presenter_email']],
                fail_silently=False,
            )

        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
